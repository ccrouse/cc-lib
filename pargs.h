/**
 * Command Line Argument Parser
 * @author Charlie Crouse
 * @version 1.0.0
 * @since 2017-08-13
 * @requires hashtable.h
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * This file is a part of a collection of C programs that construct a library  *
 *  meant for personal use.                                                    *
 *                                                                             *
 *  Copyright (C) 2017 Charlie T. Crouse                                       *
 *                                                                             *
 *  This program is free software: you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published by       *
 *  the Free Software Foundation, either version 3 of the License, or          *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  This program is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**
 * Sample Command:
 * ./prog --opt1=red --opt2=3 -xvf config
 *
 * Gives the following hashtable when parsed:
 * 
 * # - - - - - - - - - - #
 * # "opt1"   => "red"   #
 * # "opt2"   => "3"     #
 * # "x"      => "true"  #
 * # "v"      => "true"  #
 * # "f"      => "true"  #
 * # "config" => "true"  #
 * # - - - - - - - - - - #
 *
 */

#ifndef PARGS_H
#define PARGS_H

#include <stdio.h>
#include <stdlib.h>
#include "hashtable.h"

#define SIZE 2039

typedef hashtable pargv;

/**
 * allocates a new pargv buffer
 * @return the new buffer
 */
static pargv *pargv_init();

/**
 * deallocates the given pargv buffer
 * @param p A pointer to an initialized pargv
 */
void pargv_free(pargv *p);

/**
 * parses the given command line args
 * @param argv The command line arguments
 */
pargv *pargv_parse(char **argv);

/**
 * gets the value of the given option from the given parsed pargv pointer
 * @param  p      A pointer to a pargv that has been parsed
 * @param  option The option to query
 * @return the value of the given option or NULL if no value has been set
 */
char *pargv_get(pargv *p, char *option)


// ---------------------------- Implementations ----------------------------- //

// Helper functions
size_t _strlen(char *s);

int _strncmp(char *s1, char *s2, int n);

char *_strndup(char *s, int n);

static pargv *pargv_init()
{
  pargv *p;
  hashtable_init(&p, SIZE, string_hash, _strcmp, free_string, free_string);

  return p;
}

void pargv_free(pargv *p)
{
  hashtable_free(p);
}

pargv *pargs_parse(char **argv)
{
  if(!argv) {
    fprintf(stderr, "  Error: 'pargs.h, pargs_parse' | argv is invalid\n");
    exit(EXIT_FAILURE);
  }

  pargv *p = pargv_init();

  while(*argv) {
    char *arg = *argv, *ptr = arg;
    int dash_count = 0;

    while(*arg == '-') {
      dash_count++;
      arg++;
    }

    if(*arg && !dash_count) {
      hashtable_insert(p, _strdup(arg), _strdup("true"));
    } else if(*arg && dash_count == 1) {
      while(*arg) {
        char *curr = _strndup(arg, 2);
        curr[1] = 0;

        hashtable_insert(p, curr, _strdup("true"));
        arg++;
      }
    } else if(*arg && dash_count == 2) {
      char *val = arg;

      int i;
      for(i = 0; *val && *val != '='; i++, val++)
        ;

      arg[i] = 0;
      val++;

      hashtable_insert(p, _strdup(arg), _strdup(val));
    } else {
      fprintf(stderr, "  Error: 'pargs.h, pargv_parse' | invalid argument: %s\n", arg);
    }

    argv++;
  }

  return p;
}

char *pargv_get(pargv *p, char *option)
{
  if(!p || !option) {
    fprintf(stderr, "  Error: 'pargs.h, pargv_get' | %s is invalid\n", !p ? "pargv" : "option");
    exit(EXIT_FAILURE);
  }

  char *got = (char *)hashtable_find(p, (void *)option);

  return got;
}

size_t _strlen(char *s)
{
  char *p = s;
  while(*p)
    p++;
  return (const unsigned char *)p - (const unsigned char *)s;
}

int _strncmp(char *s1, char *s2, int n)
{
  while(n--) {
    if(*s1++ != *s2++)
      return *(const unsigned char *)(s1 - 1) - *(unsigned char *)(s2 - 1);
  }
  return 0;
}

char *_strndup(char *s, int n)
{
  char *p = (char *)malloc(sizeof(char) * (n + 1));

  char *r = s, *q = p;

  while(*s) {
    *p++ = *s++;
  }

  *p = 0;

  return q;
}

char *_strdup(char *s)
{
  int len = strlen(s);
  return _strndup(s, len);
}

char *_strstr(char *haystack, char *needle)
{
  size_t len = _strlen(needle);

  while(*haystack) {
    if(!_strncmp(haystack++, needle, len))
      return haystack - 1;
  }

  return NULL;
}

#endif // PARGS_H
