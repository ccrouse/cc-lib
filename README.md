# CCLib
C/C++ Library for personal use.
***
## I/O

### read.h - collection of functions for reading input from files and stdin
#### $ `curl -O https://raw.githubusercontent.com/crouse0/CCLib/master/read.h`

### pargs.h - command line argument parser that returns the command line arguments in the form of a table
#### $ `curl -O https://raw.githubusercontent.com/crouse0/CCLib/master/{pargs.h,hashtable.h} -O`

## Data Structures

### hashtable.h - generic hashtable, see examples in CCLib/master/examples/hashtable/
#### $ `curl -O https://raw.githubusercontent.com/crouse0/CCLib/master/hashtable.h`

### graph.h - unweighted, undirected graph
#### $ `curl -O https://raw.githubusercontent.com/crouse0/CCLib/master/graph.h`

### wgraph.h - weighted, undirected graph
#### $ `curl -O https://raw.githubusercontent.com/crouse0/CCLib/master/wgraph.h`
